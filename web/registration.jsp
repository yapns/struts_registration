<style type="text/css">
          td.paragraph { font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,Helvetica,sans-serif; font-size:12px;}
          td.title {font-weight: bold; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,Helvetica,sans-serif; font-size:12px; float:left;width:170px;}
          td.message {color: red; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,Helvetica,sans-serif; font-size:11px;float:left;width:200px;}
       
          td.link {font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,Helvetica,sans-serif; font-size:10px;float:left;width:170px;}
 </style>
 
 <script type="text/javascript" src="${pageContext.request.contextPath}/js/dojoroot/dojo/dojo.js"></script>
 <script type="text/javascript">
  
     function proceed(){
           
            if (document.getElementById("errorMessage1").innerHTML !='' || document.getElementById("errorMessage2").innerHTML !='' ) {
        
                return false;
       
             }else if (document.getElementById("errorMessage1").innerHTML =='' && document.getElementById("errorMessage2").innerHTML =='' ) {

                 var url = "${pageContext.request.contextPath}/ProcessRegister.do" 
                                   + '?title=' + document.getElementById("title").value
                                   +'&reg_username='+ document.getElementById("reg_username").value
                                   +'&ic='+document.getElementById("ic").value
                                   +'&instagram='+document.getElementById("instagram").value
                                   +'&facebook='+document.getElementById("facebook").value
                                   +'&twitter='+document.getElementById("twitter").value;
   
                dojo.xhrPost({
                      url: url,
                    load: function(responseObject, ioArgs){
                         return proceedSuccess();  
                    },
                    error: function(response, ioArgs){
                         return response;
                    }
                 });
              
              return false;
        }
    }
    
     function proceedSuccess(){    
     
        location.href="${pageContext.request.contextPath}/RegisterSuccess.do";
    }
    
    function onRegisterClick()
    {
        var errorMessage1;
        var errorMessage2;
        var errorMessage3;
        
        var IC =document.getElementById("ic").value;
        var validateIC =document.getElementById("ic").value;
        var numberPattern = /^[0-9]+$/;  
        var i;  
        var targetNode;
        
        if (document.getElementById("title").value == '') {
            errorMessage1 = "* Please fill in the contest title.\n";
            document.getElementById("errorMessage1").innerHTML = errorMessage1;
            document.getElementById('errorMessage1').style.display='inline';
        }
         if (document.getElementById("title").value != '') {
            errorMessage1 = "";
            document.getElementById("errorMessage1").innerHTML = errorMessage1;
            document.getElementById('errorMessage1').style.display='none';
        }
        
        
        if (document.getElementById("reg_username").value == '') {
            errorMessage2 = "* Please fill in your full name.\n";
            document.getElementById("errorMessage2").innerHTML = errorMessage2;
            document.getElementById('errorMessage2').style.display='inline';
        } else {
            errorMessage2 = "";
            document.getElementById("errorMessage2").innerHTML = errorMessage2;
            document.getElementById('errorMessage2').style.display='none';
        }


        if (document.getElementById("ic").value == ''){
            errorMessage3 =  "* Please fill in your IC No. \n";
            document.getElementById("errorMessage3").innerHTML = errorMessage3;
            document.getElementById('errorMessage3').style.display='inline';
        } else {
 
            for (i=0;i<=validateIC.length;i++){
                validateIC = validateIC.replace('-','');
            }
 
             if (IC.length == 12 && numberPattern.test(IC)){
                     errorMessage3 =  "";
                     document.getElementById("errorMessage3").innerHTML = errorMessage3;
                     document.getElementById('errorMessage3').style.display='none';
                     return proceed();
             }else if (IC.length == 14 && IC.charAt(6) == '-' && IC.charAt(9) == '-' && numberPattern.test( validateIC)){
                     errorMessage3 =  "";
                     document.getElementById("errorMessage3").innerHTML = errorMessage3;
                     document.getElementById('errorMessage3').style.display='none';
                     return proceed();
             }else{
                     errorMessage3 =  "* Please provide a valid IC No.\n";
                     document.getElementById("errorMessage3").innerHTML = errorMessage3;
                     document.getElementById('errorMessage3').style.display='inline';
                     return false;
             }     
        }
      
    }
    
</script>

<html:html>
 <head>
        <title>Registration</title>
 </head>
 <body>
      <table align="left" width="485px" border="0px" cellpadding="0" cellspacing="0" >
         
               <tr>
                  <td class="title">&nbsp</td>
               </tr>
          
               <tr>
                  <td class="paragraph">&nbsp; Register here to take part:</td>
               </tr>
          
               <tr>
                  <td class="title">&nbsp</td>
               </tr>
               
               <tr>
                   <td class="title">&nbsp; Title:</td>
                   <td class="title">    
                           <input name="title" id="title" type="text" value="${title}" />
                    </td>
               </tr>
               <tr>
                    <td class="title">&nbsp;</td>
                    <td class="message">
                            <p style="display:none;" id="errorMessage1" for="errorSignupMessage" ></p>
                     </td>
                </tr>
          
                 <tr>
                   <td class="title">&nbsp; Full Name: </td>
                   <td class="title">    
                           <input name="reg_username" id="reg_username" type="text" value="${reg_username}" maxlength="20"  />
                    </td>
                </tr>
                <tr>
                    <td class="title">&nbsp;</td>
                    <td class="message">
                            <p style="display:none;" id="errorMessage2" for="errorSignupMessage" ></p>
                     </td>
                 </tr>
                 
                  <tr>
                   <td class="title">&nbsp; IC No:  </td>
                   <td class="title">    
                            <input name="ic" id="ic" type="text" class="inputText" value="${ic}" />
                    </td>
                </tr>
                <tr>
                    <td class="title">&nbsp;</td>
                    <td class="message">
                            <p style="display:none;" id="errorMessage3" for="errorSignupMessage" ></p>
                     </td>
                 </tr>
                 
                 <tr>
                        <td class="title">&nbsp; Instagram Username: </td>
                        <td class="title">
                            <input name="instagram" id="instagram" type="text" value="${instagram}" maxlength="60"/>
                        </td>
                 </tr>
                 
                <tr>
                  <td class="title">&nbsp</td>
               </tr>
               </tr>
               
                 <tr>
                        <td class="title">&nbsp; Facebook Username: </td>
                        <td class="title">
                            <input name="facebook" id="facebook" type="text" value="${facebook}" maxlength="60"/>
                        </td>
                </tr>
                
                <tr>
                  <td class="title">&nbsp</td>
               </tr>
               
                 <tr>
                        <td class="title">&nbsp; Twitter Username: </td>
                        <td class="title">
                            <input name="twitter" id="twitter" type="text" value="${twitter}" maxlength="60"/>
                        </td>
                 </tr>
                  
                <tr>
                  <td class="title">&nbsp</td>
               </tr>
               </tr>
               
                <tr>
                    <td>
                       &nbsp;  <input type="image" name="submit" src="/WebApplication1/images/register.jpg" onclick="return onRegisterClick();" />
                    </td>
                </tr>   
                
                 <tr>
                  <tr>
                  <td class="title">&nbsp</td>
               </tr>
                
               </tr>
           
          </table>
  
 </body>
</html:html>