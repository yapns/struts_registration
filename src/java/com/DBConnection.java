package com;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.logicalcobwebs.proxool.configuration.PropertyConfigurator;

public class DBConnection {

    private static Logger logger = Logger.getLogger(DBConnection.class.getName());
    private static ResourceBundle resource = null;
    private static boolean isDBPoolInit = false;

    public DBConnection(ResourceBundle resource_) throws Exception {
        resource = resource_;
        if (!(isDBPoolInit)) {
            init(resource);
        }
    }

    public Connection getConnection()
            throws Exception {
        return DriverManager.getConnection("proxool." + resource.getString("jdbc-1.proxool.alias"));
    }

    public static synchronized void init(ResourceBundle resource_) throws Exception {
        if (!(isDBPoolInit)) {
            try {
                logger.debug("#Initializing connection pool# start...");
                Properties dbPoolProperties = new Properties();
                dbPoolProperties.setProperty("jdbc-1.proxool.alias", resource_.getString("jdbc-1.proxool.alias"));
                dbPoolProperties.setProperty("jdbc-1.proxool.driver-url", resource_.getString("jdbc-1.proxool.driver-url"));
                dbPoolProperties.setProperty("jdbc-1.proxool.driver-class", resource_.getString("jdbc-1.proxool.driver-class"));
                dbPoolProperties.setProperty("jdbc-1.user", resource_.getString("jdbc-1.user"));
                dbPoolProperties.setProperty("jdbc-1.password", resource_.getString("jdbc-1.password"));
                dbPoolProperties.setProperty("jdbc-1.proxool.house-keeping-sleep-time", resource_.getString("jdbc-1.proxool.house-keeping-sleep-time"));
                dbPoolProperties.setProperty("jdbc-1.proxool.house-keeping-test-sql", resource_.getString("jdbc-1.proxool.house-keeping-test-sql"));
                dbPoolProperties.setProperty("jdbc-1.proxool.maximum-connection-count", resource_.getString("jdbc-1.proxool.maximum-connection-count"));
                dbPoolProperties.setProperty("jdbc-1.proxool.minimum-connection-count", resource_.getString("jdbc-1.proxool.minimum-connection-count"));
                dbPoolProperties.setProperty("jdbc-1.proxool.maximum-connection-lifetime", resource_.getString("jdbc-1.proxool.maximum-connection-lifetime"));
                dbPoolProperties.setProperty("jdbc-1.proxool.simultaneous-build-throttle", resource_.getString("jdbc-1.proxool.simultaneous-build-throttle"));
                dbPoolProperties.setProperty("jdbc-1.proxool.recently-started-threshold", resource_.getString("jdbc-1.proxool.recently-started-threshold"));
                dbPoolProperties.setProperty("jdbc-1.proxool.overload-without-refusal-lifetime", resource_.getString("jdbc-1.proxool.overload-without-refusal-lifetime"));
                dbPoolProperties.setProperty("jdbc-1.proxool.maximum-active-time", resource_.getString("jdbc-1.proxool.maximum-active-time"));
                dbPoolProperties.setProperty("jdbc-1.proxool.verbose", resource_.getString("jdbc-1.proxool.verbose"));
                dbPoolProperties.setProperty("jdbc-1.proxool.trace", resource_.getString("jdbc-1.proxool.trace"));
                dbPoolProperties.setProperty("jdbc-1.proxool.fatal-sql-exception", resource_.getString("jdbc-1.proxool.fatal-sql-exception"));
                dbPoolProperties.setProperty("jdbc-1.proxool.prototype-count", resource_.getString("jdbc-1.proxool.prototype-count"));
                PropertyConfigurator.configure(dbPoolProperties);
                logger.debug("#Initializing connection pool# success...");
                isDBPoolInit = true;
            } catch (Exception e) {
                logger.error("#Initializing connection pool# Exception["+ e.getMessage() +"]");
                isDBPoolInit = false;
                throw e;
            }
        }
    }
}
