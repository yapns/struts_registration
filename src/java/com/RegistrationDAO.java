/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dao;

import com.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.ArrayList;

public class RegistrationDAO {

    private static Logger logger = Logger.getLogger(RegistrationDAO.class.getName());
    private static ResourceBundle SQL = ResourceBundle.getBundle("com.RegistrationDAO");
    private static DBConnection db;
    private static final String SQL_INSERT_REGISTRATION_INFO = SQL.getString("sql.insert.registration.info");

     public static int insertRegistrationInfo (String title, String name, String ic, String instagram, String facebook, String twitter) throws Exception {
        logger.info("# insertRegistrationInfo# Tilte["+title+"], Full Name["+name+"], ICNumber ["+ic+"], InstagramUsername["+instagram+"], FacebookUsername["+facebook+"], TwitterUsername["+twitter+"]");

        db = new DBConnection(ResourceBundle.getBundle("com.MOAPP"));
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int rowInserted = 0;

        try {
            conn = db.getConnection();
            pstmt = conn.prepareStatement(SQL_INSERT_REGISTRATION_INFO);
            pstmt.clearParameters();
            int pIndex = 1;
            pstmt.setString(pIndex++, title);
            pstmt.setString(pIndex++, name);
            pstmt.setString(pIndex++, ic);
            pstmt.setString(pIndex++, instagram);
            pstmt.setString(pIndex++, facebook);
            pstmt.setString(pIndex++, twitter);
            java.util.Date utilDate = new java.util.Date();
            // Convert it to java.sql.Date
            java.sql.Timestamp CreateDate = new java.sql.Timestamp(utilDate.getTime());
            pstmt.setTimestamp(pIndex++, CreateDate);

            rowInserted = pstmt.executeUpdate();

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.error("insertRegistrationInfo - SQLException: {}", e);
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    logger.error("insertRegistrationInfo - SQLException: {}", e);
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    logger.error("insertRegistrationInfo - SQLException: {}", e);
                }
                conn = null;
            }
        }
        return rowInserted;
    }
 
}
